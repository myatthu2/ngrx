
import { identifierName } from "@angular/compiler";
import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, mergeMap } from "rxjs";
import { PostService } from "src/app/services/post.service";
import { Addpost, Addpostuccess, DeletePost, DeletePostsuccess, LoadPosts, LoadPostsSuccess, UpdatePost, UpdatePostSuccess } from "./post.action";

@Injectable()
export class PostEffects{
    constructor(private actions$ :Actions,private postService :PostService){

    }
    loadPost$ = createEffect(()=>{
        return this.actions$.pipe(
        ofType(LoadPosts),
        mergeMap((action)=>{
           return this.postService.getPosts().pipe(
                map((posts)=>{
                  return LoadPostsSuccess({posts}) 
                })
            )
        })
        )
    });
    addPost$ = createEffect(() => {
        return this.actions$.pipe(
          ofType(Addpost),
          mergeMap((action) => {
            return this.postService.addPost(action.post).pipe(
              map((data) => {
                const post = { ...action.post, id: data.name };
                return Addpostuccess({ post });
              })
            );
          })
        );
      });


      updatePost$ = createEffect(()=>{
        return this.actions$.pipe(
            ofType(UpdatePost),
            mergeMap((action)=>{
             return   this.postService.updatePost(action.post).pipe(
                    map((data)=>{
                        return UpdatePostSuccess({post:action.post})
                    })
                )
            })
        )
      })

      deletePost$ = createEffect(()=>{
        return this.actions$.pipe(
          ofType(DeletePost),
          mergeMap((action)=>{
            return this.postService.deletePost(action.id).pipe(
              map((data)=>{
                return DeletePostsuccess({id:action.id})
              })
            )
          })
        )
      })
}