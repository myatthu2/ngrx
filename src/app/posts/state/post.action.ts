import { createAction, props } from "@ngrx/store";
import { Post } from "src/app/models/post.model";


export const Addpost = createAction('Addpost', props<{post:Post}>());
export const UpdatePost = createAction('UpdatePost', props<{post:Post}>());
export const DeletePost = createAction('DeletePost',props<{id:string}>());
export const LoadPosts = createAction('LoadPosts');
export const LoadPostsSuccess= createAction('LoadPostsSuccess',props<{posts:Post[]}>());
export const Addpostuccess = createAction('Addpostuccess',props<{post:Post}>());
export const UpdatePostSuccess =  createAction('UpdatePostSuccess',props<{post:Post}>());
export const DeletePostsuccess = createAction('DeletePostsuccess',props<{id:string}>());