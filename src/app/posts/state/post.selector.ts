import { createFeatureSelector, createSelector } from "@ngrx/store";
import { PostState } from "./post.state";
export const Post_State_Name = 'posts'
const getPostState = createFeatureSelector<PostState>('Post_State_Name');
export const getPost = createSelector(getPostState,(state)=>{
    return state.posts;
});
export const getPosId = createSelector(getPostState,(state:any, props:any)=>{
    return state.posts.find((posts:any)=> posts.id == props.id);
})