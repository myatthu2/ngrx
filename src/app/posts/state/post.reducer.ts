import { Action, createReducer, on } from "@ngrx/store";


import {  Addpostuccess, DeletePost, DeletePostsuccess, LoadPostsSuccess, UpdatePost, UpdatePostSuccess } from "./post.action";

import { initialState } from "./post.state";

const _postrReducer = createReducer(initialState,
    on(Addpostuccess, (state,action)=>{
        let post = {...action.post};
       

        return{
            ...state,
             posts:[...state.posts,post]

        }
    }),
    on(UpdatePostSuccess,(state:any,action:any)=>{
        const updatPosts = state .posts.map((post:any)=>{
            return action.post.id === post.id ? action.post :post;
        }) 
        return{
            ...state,
            posts: updatPosts,
        }
    }),
    on(DeletePostsuccess,(state,{id})=>{
    const deletePost = state.posts.filter(post=>{
        return post.id !== id;
    })
        return{
            ...state,
            posts:deletePost,
        }
    }),
    on(LoadPostsSuccess,(state,action)=>{
        return{
            ...state,
            posts:action.posts,
        }
    })
   
    )
    export function postReducer(state:any, action:Action){
        return _postrReducer(state,action);

    }