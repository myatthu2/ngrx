import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { Post } from 'src/app/models/post.model';
import { AppState } from 'src/app/store/app.state';
import { Addpost } from '../state/post.action';

@Component({
  selector: 'app-addpost',
  templateUrl: './addpost.component.html',
  styleUrls: ['./addpost.component.css'],
})
export class AddpostComponent implements OnInit {
  frmPost: any;
  submit: boolean = false;
  erros: any;

  constructor(
    private frmBuilder: FormBuilder,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.frmPost = this.frmBuilder.group({
      title: ['', [Validators.required, Validators.minLength(6)]],
      discription: ['', [Validators.required]],
    });
  }
  get validation() {
    return this.frmPost.controls;
  }

  onAddPost() {
    this.submit = true;
   
      const post: Post = {
        title: this.frmPost.value.title,
        discription: this.frmPost.value.title,
        id: ''
      };
      this.store.dispatch(Addpost({ post }));
 
  }
}
