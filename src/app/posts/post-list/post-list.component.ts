import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Post } from 'src/app/models/post.model';
import { AppState } from 'src/app/store/app.state';
import { DeletePost, LoadPosts } from '../state/post.action';
import { getPost } from '../state/post.selector';


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
})
export class PostListComponent implements OnInit {
  posts!: Observable<Post[]>;
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.posts = this.store.select(getPost);
    this.store.dispatch(LoadPosts());
  }
  deletePost(id:string) {
    if (confirm('Are u sure to delete post')) {
      this.store.dispatch(DeletePost({ id }));
    }
  }
}
