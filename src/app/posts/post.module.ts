

import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { RouterModule, Routes } from "@angular/router";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";

import { AddpostComponent } from "./addpost/addpost.component";
import { EditPostComponent } from "./edit-post/edit-post.component";
import { PostListComponent } from "./post-list/post-list.component";
import { PostEffects } from "./state/post.effects";
import { postReducer } from "./state/post.reducer";




const routes: Routes=[
    {path:'',
    component:PostListComponent,
    children:[
     {path:'add',component:AddpostComponent},
     {path:'edit/:id' ,component:EditPostComponent}
 
   ]}
   
];
@NgModule({
    declarations: [
        PostListComponent,
        AddpostComponent,
        EditPostComponent
    ],
    imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
     RouterModule.forChild(routes),
     StoreModule.forFeature('Post_State_Name',postReducer),
     EffectsModule.forFeature([PostEffects]),

     
    ],
    providers: [],
    bootstrap: []
  })

export class PostModule{



}