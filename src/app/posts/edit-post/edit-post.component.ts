import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Post } from 'src/app/models/post.model';
import { AppState } from 'src/app/store/app.state';
import { UpdatePost } from '../state/post.action';
import { getPosId } from '../state/post.selector';


@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css'],
})
export class EditPostComponent implements OnInit, OnDestroy {
  post!: Post;
  PostForm!: FormGroup;
  PostSub!: Subscription;
  constructor(
    private ActRoute: ActivatedRoute,
    private store: Store<AppState>,
    private route :Router,
  ) {}
  ngOnDestroy(): void {
    if (this.PostSub) {
      this.PostSub.unsubscribe();
    }
  }
  ngOnInit(): void {
    this.ActRoute.paramMap.subscribe((params) => {
      const id = params.get('id');
      this.PostSub = this.store.select(getPosId, { id }).subscribe((data) => {
        this.post = data;
        
        this.createForm();
      });
    });
  }
  createForm() {
    this.PostForm = new FormGroup({
      title: new FormControl(this.post.title, [Validators.required]),
      description: new FormControl(this.post.discription, [
        Validators.required,
      ]),
    });
  }

  onUpdate() {
    if (!this.PostForm.valid) {
      return;
    }
    const title = this.PostForm.value.title;
    const discription = this.PostForm.value.description;
    const post: Post = {
      id: this.post.id,
      title,
      discription,
    };
    this.store.dispatch(UpdatePost({ post }));
    this.route.navigateByUrl('/posts');
  }
}
