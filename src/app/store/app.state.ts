
import { AuthReducer } from "../auth/state/auth.reducer";
import { Auth_State_Name } from "../auth/state/auth.selector";
import { AuthState } from "../auth/state/auth.state";
import { shareReducer } from "./Shared/shared.reducer";
import { SHARED_STATE_NAME } from "./Shared/shared.selector";
import { SharedStae } from "./Shared/shared.state";

export interface AppState{
   [SHARED_STATE_NAME]:SharedStae;
   [Auth_State_Name] :AuthState;
}
export const AppReducer={
    [SHARED_STATE_NAME]:shareReducer,
    [Auth_State_Name]:AuthReducer,
}