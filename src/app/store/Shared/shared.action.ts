import { createAction, props } from "@ngrx/store";

export const setLoadingspinner = createAction('setLoadingspinner',props<{status:boolean}>());
export const setErrorMessage = createAction('setErrorMessage',props<{message:string}>());