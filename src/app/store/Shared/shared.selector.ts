import { state } from "@angular/animations";
import { createFeatureSelector, createSelector } from "@ngrx/store";
import { SharedStae } from "./shared.state";

export const SHARED_STATE_NAME = 'share';
const getShareState = createFeatureSelector<SharedStae>(SHARED_STATE_NAME);
export const getLoading = createSelector(getShareState,(state)=>{
    return state.showloading;
});
export const getErrorMessage = createSelector(getShareState,(state)=>{
    return state.errorMessage;
})
