import { Action, createReducer, on } from "@ngrx/store";
import { setErrorMessage, setLoadingspinner } from "./shared.action";
import { initialState } from "./shared.state";

const _shareReducer= createReducer(initialState,
    on(setLoadingspinner, (state,action)=>{

     return{
        ...state,
        showloading:action.status,
   }
    }),
    on(setErrorMessage,(state,action)=>{
        return{
            ...state,
            errorMessage:action.message,
        }
    })
    )
export function shareReducer(state:any,action:Action){
    return _shareReducer(state,action);
}