import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouterModule, Routes } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';

import { LoginComponent } from './login/login.component';
import { AuthEffects } from './state/auth.effects';
import { SingupComponent } from './singup/singup.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    // children:[
    //     {path:'', redirectTo:'login'},
    //     {path:'login',component:LoginComponent}
    // ]
  },
  {path:'singup',component:SingupComponent}
];
@NgModule({
  declarations: [LoginComponent, SingupComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    EffectsModule.forFeature(),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [],
})
export class AuthModule {}
