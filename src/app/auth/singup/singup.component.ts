import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.state';
import { setLoadingspinner } from 'src/app/store/Shared/shared.action';
import { Singupstart } from '../state/auth.action';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css'],
})
export class SingupComponent implements OnInit {
  singUpform: any;
  submit: boolean = false;
  constructor(
    private frmBuilder: FormBuilder,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.singUpform = this.frmBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }
  get validation() {
    return this.singUpform.controls;
  }
  singup() {
    this.submit = true;
    console.log(this.singUpform.value);
    const email = this.singUpform.value.email;
    const password = this.singUpform.value.password;
    this.store.dispatch(setLoadingspinner({status:true}))
    this.store.dispatch(Singupstart({email,password}));
  }
}
