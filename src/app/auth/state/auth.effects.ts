import { Injectable } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, exhaustMap, map, mergeMap, of, tap } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { AppState } from 'src/app/store/app.state';
import {
  setErrorMessage,
  setLoadingspinner,
} from 'src/app/store/Shared/shared.action';
import {
  AutoLogin,
  autoLogOut,
  LoginStart,
  LoginSuccess,
  Singupstart,
  SingupSuccess,
} from './auth.action';

@Injectable()
export class AuthEffects {
  constructor(
    private action$: Actions,
    private authservice: AuthService,
    private store: Store<AppState>,
    private router: Router
  ) {}
  login$ = createEffect(() => {
    return this.action$.pipe(
      ofType(LoginStart),
      exhaustMap((action) => {
        return this.authservice.login(action.email, action.password).pipe(
          map((data) => {
           
            this.store.dispatch(setLoadingspinner({ status: false }));
            this.store.dispatch(setErrorMessage({ message: '' }));
            const user = this.authservice.formatUser(data);
            this.authservice.setUserInLocalStorage(user);
            return LoginSuccess({ user,redirect:true });
          }),
          catchError((errResp) => {
            this.store.dispatch(setLoadingspinner({ status: false }));
            console.log(errResp.error.error.message);
            const errorMessage = this.authservice.getErrorMessage(
              errResp.error.error.message
            );
            return of(setErrorMessage({ message: errorMessage }));
          })
        );
      })
    );
  });

  loginRedirect$ = createEffect(
    () => {
      return this.action$.pipe(
        ofType(...[LoginSuccess,SingupSuccess]),
        tap((action) => {
          
          this.store.dispatch(setErrorMessage({message:''}));
          if(action.redirect){
            this.router.navigate(['/']);
          }
         
        })
      );
    },
    { dispatch: false }
  );
  //    

  singup$ = createEffect(() => {
    return this.action$.pipe(
      ofType(Singupstart),
      exhaustMap((action) => {
        return this.authservice.singup(action.email, action.password).pipe(
          map((data) => {
            this.store.dispatch(setLoadingspinner({ status: false }));
            
            const user = this.authservice.formatUser(data);
           this.authservice.setUserInLocalStorage(user);
            return SingupSuccess({ user,redirect:true });
          }),
          catchError((errRes) => {
            this.store.dispatch(setLoadingspinner({ status: false }));
            const errorMessage = this.authservice.getErrorMessage(
              errRes.error.error.message
            );
            return of(setErrorMessage({ message: errorMessage }));
          })
        );
      })
    );
  });

  
  autoLogin$ = createEffect(() => {
    return this.action$.pipe(
      ofType(AutoLogin),
      mergeMap(() => {
        const user = this.authservice.getUserFromLocalStorage();
        return of(LoginSuccess({user,redirect:false}))
      })
    );
  });
  logOut$ = createEffect(()=>{
    return this.action$.pipe(ofType(autoLogOut),
    map(()=>{
      this.authservice.logOut();
      this.router.navigate(['auth']);
    })
    )
  },{dispatch:false})
}
