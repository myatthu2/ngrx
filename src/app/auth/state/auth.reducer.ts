import { Action, createReducer, on } from "@ngrx/store";
import { SingupComponent } from "../singup/singup.component";
import { autoLogOut, LoginSuccess, Singupstart, SingupSuccess } from "./auth.action";

import { initialState } from "./auth.state";

const _authReducer= createReducer(initialState,
    on(LoginSuccess,(state,action)=>{
        console.log(action);
        return{
            ...state,
            user: action.user,
        }
    }),
    on(SingupSuccess,(state,action)=>{
        return{
            ...state,
            user:action.user,
        }
    }),
    on(autoLogOut,(state)=>{
        return {
            ...state,
            user:null,
        }
    })
    )

    export function AuthReducer(state:any,action:Action){
        return _authReducer(state,action);
    }