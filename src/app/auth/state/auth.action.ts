import { createAction, props } from "@ngrx/store";
import { User } from "src/app/models/user.model";



export const LoginStart =createAction('LoginStart',props<{email:string;password:string}>());
export const LoginSuccess=createAction('Loginsuccess',props<{user:User,redirect:boolean}>());
export const LoginFail =createAction('LoginFail');

export const  Singupstart = createAction('Singupstart', props<{email:string,password:string}>());
export const SingupSuccess =createAction('SingupSuccess',props<{user:User,redirect:boolean}>());
export const AutoLogin = createAction('AutoLogin');
export const autoLogOut = createAction('autoLogOut')
