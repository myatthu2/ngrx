import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store/app.state';
import { changeChannelName, customIncrement } from '../state/counter.action';
import { getChannelName } from '../state/counter.selector';
import { CounterState } from '../state/counter.state';




@Component({
  selector: 'app-custom-counter',
  templateUrl: './custom-counter.component.html',
  styleUrls: ['./custom-counter.component.css']
})
export class CustomCounterComponent implements OnInit {
 value!: number;
 channelName$!: Observable<string>
  constructor(private store: Store<AppState>) {
   
    
   }

  ngOnInit(): void {
   this.channelName$= this.store.select(getChannelName);
      
   
  } 
  onAdd(){
this.store.dispatch(customIncrement({count: +this.value}))
 }
 onChangeChannelName(){
  this.store.dispatch(changeChannelName());
 }

}
