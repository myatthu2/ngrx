import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { autoLogOut } from 'src/app/auth/state/auth.action';
import { isAuthenicated } from 'src/app/auth/state/auth.selector';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isAuthenicated!:Observable<boolean>;
  constructor(private store : Store<AppState>) { }

  ngOnInit(): void {
    this.isAuthenicated= this.store.select(isAuthenicated)
  }
  LogOut(event :Event){
    event.preventDefault();
    this.store.dispatch(autoLogOut())

  }

}
