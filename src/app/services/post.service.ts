import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { map, Observable } from 'rxjs';
import { Post } from '../models/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http:HttpClient) {
    
   }
   getPosts(): Observable<Post[]> {
    return this.http
      .get<Post[]>(`https://myproject2-5b156-default-rtdb.firebaseio.com/posts.json`)
      .pipe(
        map((data) => {
          const posts: Post[] = [];
          for (let key in data) {
            posts.push({ ...data[key], id: key });
          }
          return posts;
        })
      );
  }
  addPost(post:Post):Observable<{name:string}>{
    return this.http.post<{name:string}>(`https://myproject2-5b156-default-rtdb.firebaseio.com/posts.json`,post)
  }
  updatePost(post:Post){
    const PostData = {[post.id] :{title : post.title,discription :post.discription}};
    return this.http.patch(`https://myproject2-5b156-default-rtdb.firebaseio.com/posts.json`,PostData);
    
  }
  deletePost(id:string){
    return this.http.delete(`https://myproject2-5b156-default-rtdb.firebaseio.com/posts/${id}.json`);
  }
}
