import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { autoLogOut } from '../auth/state/auth.action';
import { AuthResponseData } from '../models/AuthResponseData.model';
import { User } from '../models/user.model';
import { AppState } from '../store/app.state';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  timeoutInterval:any;

  constructor(private http:HttpClient,
    private store: Store<AppState>) {

   }
   login(email:string,password:string):Observable<AuthResponseData>{
    
    return this.http.post<AuthResponseData>(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.FIRBASE_API_KEY}`,{email,password,returnSecureToken:true})
   }
   singup(email:string,password:string):Observable<AuthResponseData>{
    return this.http.post<AuthResponseData>(`https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${environment.FIRBASE_API_KEY}`,{email,password,returnSecureToken:true})

   }


   formatUser(data:AuthResponseData){
    const expriationDate =  new Date(new Date().getTime() + +data.expiresIn * 1000)
    const user  = new User(data.email,data.idToken,data.localId,expriationDate);
    return user;

   }
   getErrorMessage(message:string){
    switch(message){
      case'EMAIL_NOT_FOUND':
      return 'Email Not Found';
      case 'INVALID_PASSWORD':
        return 'Invalid Password';
        case 'EMAIL_EXISTS':
          return 'Email are alreay exists';
          
      
       default:
        return 'User Disbaled'
    }
   }
   setUserInLocalStorage(user:User){
     localStorage.setItem('userData',JSON.stringify(user));
     
     this.runTimeoutInterval(user);
//  const todayDate = new Date().getTime();
//      const expriationDate = user.experieDate.getTime();
//      const timeInterval = expriationDate - todayDate;
//     this.timeoutInterval= setTimeout(()=>{
//       //logout function or get the refresh token
//      },timeInterval)
   }
   runTimeoutInterval(user:User){
    const todayDate = new Date().getTime();
     const expriationDate = user.experieDate.getTime();
     const timeInterval = expriationDate - todayDate;
    this.timeoutInterval= setTimeout(()=>{
      this.store.dispatch(autoLogOut());
      //logout function or get the refresh token
     },timeInterval)
   }
   getUserFromLocalStorage() {
    const userDataString = localStorage.getItem('userData');
    if (userDataString) {
      const userData = JSON.parse(userDataString);
      const expirationDate = new Date(userData.expirationDate);
      const user = new User(
        userData.email,
        userData.token,
        userData.localId,
        expirationDate
      );
      this.runTimeoutInterval(user);
      return user;
    }
    return null as any;
  }
   logOut(){
    localStorage.removeItem('userData');
    if(this.timeoutInterval){
      clearTimeout(this.timeoutInterval);
      this.timeoutInterval= null;
    }
   }
  }
